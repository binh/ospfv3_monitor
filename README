Copyright Binh Nguyen - University of Utah (binh@cs.utah.edu).

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=================================================================
OSPFv3 Monitor and Parser
=================================================================

This program captures and parses all OSPFv3 (IPV6) messages. It could be used to get link state information on an OSPF network for real-time network topology monitoring and doing traffic engineering.

This program is built based on the Pyrt code (https://github.com/mor1/pyrt.git). However, it supports OSPFv3.

=================================================================
How to run?
	1. Modify 'LSAA_HOST' and 'LSAA_PORT' in the main.py to point to the listener of the collected OSPFv3 information (see https://gitlab.flux.utah.edu/binh/ovs-srv6.git for information about the OSPFv3 listener).
	2. Start capturing OSPF LSAs by: sudo python main.py

=================================================================
Contact: Binh Nguyen, binh@cs.utah.edu
